
from functions.ingester import *
from functions.saga_functions import *
from functions.chelsa_functions import *
from functions.chelsa_data_classes import *
from functions.chelsa import chelsa


debugging=1

### Parameters for local debugging
if debugging==1:
    year    = "2041"
    month   = "01"
    day     = "01"
    month1  = "1"
    INPUT   = '/storage/karger/chelsa_V2/INPUT/'
    TEMP    = '/home/karger/scratch/'
    SRAD    = '/storage/karger/chelsa_V2/OUTPUT_DAILY/srad/'
    CC      = '/storage/karger/chelsa_V2/climatologies/1981-2010/tcc/'
    W5E5    = '/storage/karger/CMIP6_AUX/'
    #CCOVER  = '/mnt/storage/karger/chelsa_V2/OUTPUT_DAILY/tcc/'
    ISIMIP  = '/storage/karger/ISIMIP3b/'
    ISIMIP_PR  = '/storage/karger/ISIMIP3b/'
    CMIP6   = '/storage/dabaghch/cmip6/'
    MODEL   = 'MPI-ESM1-2-HR'
    RCP     = 'ssp126'
    lev_low  =  100000
    lev_high =  85000
    OUTPUT  = '/storage/karger/chelsa_V2/ISIMIP3b_BA/' + MODEL + '/' + RCP + '/'
    TEMP = TEMP + '/' + year + month + day + '/'
    dayofyear = "1"
    calcprec=1
    t='1'
    ISIMIP = ISIMIP + RCP + '/' + MODEL + '/'
    ISIMIP_PR  = ISIMIP_PR + RCP + '/' + MODEL + '/'

    saga_api.SG_Get_Data_Manager().Delete_All()  #
    Load_Tool_Libraries(True)

    ### ingest the data
    ingest(isimip=ISIMIP,
           isimip_pr=ISIMIP_PR,
           cmip=CMIP6,
           temp=TEMP,
           model=MODEL,
           rcp=RCP,
           day=day,
           month=month,
           year=year,
           levlow=lev_low,
           levhigh=lev_high).ingest_data()

    ### create the data classes
    coarse_data = Coarse_data(TEMP=TEMP,
                              lev_high=lev_high,
                              lev_low=lev_low)

    dem_data = Dem_data(INPUT=INPUT)

    aux_data = Aux_data(INPUT=INPUT,
                        W5E5=W5E5)

    cc_downscale_data = Cc_downscale_data(CC=CC,
                                          TEMP=TEMP,
                                          month=month)

    srad_data = Srad_data(SRAD=SRAD,
                          dayofyear=dayofyear)


    ### calculate windeffect
    windef1 = calculate_windeffect(Coarse=coarse_data,
                                   Dem=dem_data)


    ### correct windeffect
    wind_cor, wind_coarse25, windcoarse = correct_windeffect(windef1=windef1,
                                                             Coarse=coarse_data,
                                                             Dem=dem_data,
                                                             Aux=aux_data)

    ### clean up memory
    wind_cor.Save(TEMP + 'wind_cor.sgrd')
    windcoarse.Save(TEMP + 'wincoarse.sgrd')
    wind_coarse25.Save(TEMP + 'wind_coarse25.sgrd')

    saga_api.SG_Get_Data_Manager().Delete_All()

    wind_cor = load_sagadata(TEMP + 'wind_cor.sgrd')
    windcoarse = load_sagadata(TEMP + 'wincoarse.sgrd')
    wind_coarse25 =load_sagadata(TEMP + 'wind_coarse25.sgrd')

    ### downscale precipitation
    pr = precipitation(wind_cor=wind_cor,
                       wind_coarse=windcoarse,
                       wind_coarse25=wind_coarse25,
                       Coarse=coarse_data,
                       Aux=aux_data)

    ### downscale cloud cover
    cc_fin = cloudcover(Cc_downscale_data=cc_downscale_data,
                        Aux=aux_data)

    ### downscale tas
    tas = temperature(Coarse=coarse_data,
                      Dem=dem_data,
                      Aux=aux_data,
                      var='tas')

    rsds = solar_radiation(Srad=srad_data,
                           Coarse=coarse_data,
                           cc_fin=cc_fin)
