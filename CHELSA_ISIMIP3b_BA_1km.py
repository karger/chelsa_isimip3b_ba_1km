#!/usr/bin/env python

#This file is part of chelsa_isimip3b_ba_1km.
#
#chelsa_isimip3b_ba_1km is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#chelsa_isimip3b_ba_1km is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with chelsa_isimip3b_ba_1km.  If not, see <https://www.gnu.org/licenses/>.

# ***************************************
# import libraries
# ***************************************

import saga_api
import sys
import os
import argparse
import datetime
import os.path
import cdsapi
import psutil
import shutil
import xarray as xr

# *************************************************
# import functions and classes
# *************************************************

from functions.ingester import *
from functions.saga_functions import *
from functions.chelsa_functions import *
from functions.chelsa_data_classes import *
from functions.chelsa import chelsa

# *************************************************
# global parameters
# *************************************************

process = psutil.Process(os.getpid())
saga_api.SG_Set_History_Depth(0)

# *************************************************
# Get the command line arguments
# *************************************************
ap = argparse.ArgumentParser(
    description='''# This python code is adapted for CHELSA_V2.1_ISIMIP3b
the code is adapted to the ISIMIP3b_BA data. It runs the CHELSA algorithm for 
min-, max-, and mean temperature, total downwelling solar radiation, and 
surface precipitation. The output directory needs the following 
subfolders: /rsds, /pr, /tasmin, /tasmax, /tas.
Dependencies for ubuntu_18.04:
libwxgtk3.0-dev libtiff5-dev libgdal-dev libproj-dev 
libexpat-dev wx-common libogdi3.2-dev unixodbc-dev
g++ libpcre3 libpcre3-dev wget swig-4.0.1 python2.7-dev 
software-properties-common gdal-bin python-gdal 
python2.7-gdal libnetcdf-dev libgdal-dev
python-pip cdsapi saga_gis-7.6.0
All dependencies are resolved in the chelsa_V2.1.cont singularity container
Tested with: singularity version 3.3.0-809.g78ec427cc
''',
    epilog='''author: Dirk N. Karger, dirk.karger@wsl.ch, Version 2.1'''
)

# collect the function arguments
ap.add_argument('-b','--year', type=int, help="year, integer")
ap.add_argument('-c','--month', type=int, help="month, integer")
ap.add_argument('-d','--day', type=int,  help="day, integer")
ap.add_argument('-t1','--array_task_id', type=int, help= 'slurm array task id to identify the respective band')
ap.add_argument('-i','--input', type=str, help="input directory, string")
ap.add_argument('-o','--output', type=str,  help="output directory, string")
ap.add_argument('-t','--temp', type=str, help="root for temporary directory, string")
ap.add_argument('-s','--srad', type=str, help="srad input directory, string")
ap.add_argument('-w','--isimip', type=str, help="isimip input directory, string")
ap.add_argument('-pr','--isimip_pr', type=str, help="isimip input directory for pr, string")
ap.add_argument('-cm','--cmip', type=str, help="cmip input directory, string")
ap.add_argument('-au','--aux', type=str, help="directory with auxilariy files, string")
ap.add_argument('-ccf','--cloudcc', type=str, help="directory with cloud cover climatologies, string")

ap.add_argument('-m','--model', type=str, help='isimip3b model, string')
ap.add_argument('-ssp','--ssp', type=str, help='ssp, string')

ap.add_argument('-ll','--ll', type=str, help="lowest pressure level for tz calculation, string")
ap.add_argument('-lh','--lh', type=str, help="highest pressure level for tz calculation, string")


args = ap.parse_args()

# *************************************************
# Get arguments
# *************************************************
print(args)

year = "%02d" % args.year
day = "%02d" % args.day
month = "%02d" % args.month
t = "%0d"  % args.array_task_id
month1 = "%01d" % args.month
lev_low = int(args.ll)
lev_high = int(args.lh)

# get day of the year
dt = datetime.datetime(args.year,
                       args.month,
                       args.day)
tt = dt.timetuple()
dayofyear = tt.tm_yday
dayofyear = "%0d" % dayofyear

# *************************************************
# Set the directories from arguments
# *************************************************

MODEL = args.model
RCP = args.ssp
INPUT = args.input
OUTPUT = args.output
TEMP = args.temp
SRAD = args.srad
ISIMIP_BASE = args.isimip
ISIMIP = ISIMIP_BASE #+ RCP + '/' + MODEL + '/'
ISIMIP_PR = args.isimip_pr
W5E5 = args.aux
CMIP6 = args.cmip
CC = args.cloudcc
TEMP = str(TEMP + MODEL + RCP + year + month + day + '/')

if os.path.exists(TEMP) and os.path.isdir(TEMP):
    shutil.rmtree(TEMP)

os.mkdir(TEMP)

debugging=0

### Parameters for local debugging
if debugging==1:
    year    = "2041"
    month   = "01"
    day     = "01"
    month1  = "1"
    INPUT   = '/storage/karger/chelsa_V2/INPUT/'
    TEMP    = '/home/karger/scratch/'
    SRAD    = '/storage/karger/chelsa_V2/OUTPUT_DAILY/srad/'
    CC      = '/storage/karger/chelsa_V2/climatologies/1981-2010/tcc/'
    W5E5    = '/storage/karger/CMIP6_AUX/'
    #CCOVER  = '/mnt/storage/karger/chelsa_V2/OUTPUT_DAILY/tcc/'
    ISIMIP  = '/storage/karger/ISIMIP3b/'
    ISIMIP_PR  = '/storage/karger/ISIMIP3b/'
    CMIP6   = '/storage/dabaghch/cmip6/'
    MODEL   = 'MPI-ESM1-2-HR'
    RCP     = 'ssp585'
    lev_low  =  100000
    lev_high =  85000
    OUTPUT  = '/storage/karger/chelsa_V2/ISIMIP3b_BA/' + MODEL + '/' + RCP + '/'
    TEMP = TEMP + '/' + year + month + day + '/'
    dayofyear = "1"
    calcprec=1
    t='1'
    ISIMIP = ISIMIP + RCP + '/' + MODEL + '/'
    ISIMIP_PR  = ISIMIP_PR + RCP + '/' + MODEL + '/'
    os.mkdir(TEMP)


# ************************************************
# Script
# ************************************************
if __name__ == '__main__':
    saga_api.SG_Get_Data_Manager().Delete_All()  #
    Load_Tool_Libraries(True)

    ### ingest the data
    ingest(isimip=ISIMIP,
           isimip_pr=ISIMIP_PR,
           cmip=CMIP6,
           temp=TEMP,
           model=MODEL,
           rcp=RCP,
           day=day,
           month=month,
           year=year,
           levlow=lev_low,
           levhigh=lev_high).ingest_data()


    ### create the data classes
    coarse_data = Coarse_data(TEMP=TEMP,
                              lev_high=lev_high,
                              lev_low=lev_low)

    dem_data = Dem_data(INPUT=INPUT)

    aux_data = Aux_data(INPUT=INPUT,
                        W5E5=W5E5)

    cc_downscale_data = Cc_downscale_data(CC=CC,
                                          TEMP=TEMP,
                                          month=month)

    srad_data = Srad_data(SRAD=SRAD,
                          dayofyear=dayofyear)

    ### run chelsa
    tas, tasmax, tasmin, rsds, pr = chelsa(coarse_data=coarse_data,
                                           dem_data=dem_data,
                                           aux_data=aux_data,
                                           cc_downscale_data=cc_downscale_data,
                                           srad_data=srad_data,
                                           TEMP=TEMP)

    ### save the results
    outfile = OUTPUT + 'pr/CHELSA_ISIMIP3b_BA_' + MODEL + '_' + RCP + '_pr_' + str(day).zfill(
        2) + '_' + month + '_' + year + '_V.1.0.tif'
    export_geotiff(pr,
                   outfile)

    outfile = OUTPUT + 'tas/CHELSA_ISIMIP3b_BA_' + MODEL + '_' + RCP + '_tas_' + str(day).zfill(
        2) + '_' + month + '_' + year + '_V.1.0.tif'
    export_geotiff(tas,
                   outfile)

    outfile = OUTPUT + 'tasmin/CHELSA_ISIMIP3b_BA_' + MODEL + '_' + RCP + '_tasmin_' + str(day).zfill(
        2) + '_' + month + '_' + year + '_V.1.0.tif'
    export_geotiff(tasmin,
                   outfile)

    outfile = OUTPUT + 'tasmax/CHELSA_ISIMIP3b_BA_' + MODEL + '_' + RCP + '_tasmax_' + str(day).zfill(
        2) + '_' + month + '_' + year + '_V.1.0.tif'
    export_geotiff(tasmax,
                   outfile)

    outfile = OUTPUT + 'rsds/CHELSA_ISIMIP3b_BA_' + MODEL + '_' + RCP + '_rsds_' + str(day).zfill(
        2) + '_' + month + '_' + year + '_V.1.0.tif'
    export_geotiff(rsds,
                   outfile)

    # remove the temporary directory
    shutil.rmtree(TEMP)
