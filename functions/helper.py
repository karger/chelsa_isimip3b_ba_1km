#!/usr/bin/env python

#This file is part of chelsa_isimip3b_ba_1km.
#
#chelsa_isimip3b_ba_1km is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#chelsa_isimip3b_ba_1km is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with chelsa_cmip6.  If not, see <https://www.gnu.org/licenses/>.

import os
from functions.saga_functions import export_geotiff
from datetime import date

d0 = date(1850, 1, 1)
d1 = date(int(year), int(month), int(day))
delta = d1 - d0

class export_ncdf:
    """Interpolation class"""
    def __init__(self, data, filename, temp, shortname, longname, day=False, month=False, year=False, type='UInt16', unit=False, scale=1, offset=0, fillvalue=65535):
        """ Create a set of baseline clims """
        self.data = data
        self.filename = filename
        self.type = type
        self.scale = scale
        self.offset = offset
        self.shortname = shortname
        self.longname = longname
        self.fillvalue = fillvalue
        self.temp = temp
        self.day = day
        self.month = month
        self.year = year
        self.unit = unit

    def save(self):
        export_geotiff(self.data, self.temp + 'tmp1.tif')
        os.system('gdal_translate -of netCDF -co "FORMAT=NC4" -a_nodata 65535 -ot UInt16 ' + self.temp + 'tmp1.tif' + ' ' + self.temp + 'tmp2.nc')
        os.system('ncrename -v Band1,' + self.shortname + ' ' + self.temp + 'tmp2.nc')
        os.system('ncatted -a long_name,' + self.shortname + ',o,c,' + self.longname + ' ' + self.temp + 'tmp2.nc')
        os.system('ncatted -a short_name,' + self.shortname + ',o,c,' + self.shortname + ' ' + self.temp + 'tmp2.nc')
        os.system('ncatted -a _FillValue,' + self.shortname + ',o,f,' + str(self.fillvalue) + ' ' + self.temp + 'tmp2.nc')
        os.system('ncatted -O -a units,' + self.shortname + ',m,c,' + self.unit + ' ' + self.temp + 'tmp2.nc')
        os.system('ncatted -O -a scale_factor,' + self.shortname + ',o,f,' + str(self.scale) + ' ' + self.temp + 'tmp2.nc')
        os.system('ncatted -O -a add_offset,' + self.shortname + ',o,f,' + str(self.offset) + ' ' + self.temp + 'tmp2.nc')
        ##s.system('nccopy -d9 ' + self.temp + 'tmp2.nc' + ' ' + self.filename)
        os.system('cdo setday,' + str(self.day).zfill(1) + ' ' + self.temp + 'tmp2.nc' + ' ' + self.temp + 'tmp1.nc')
        os.system('cdo setmon,' + str(self.month).zfill(1) + ' ' + self.temp + 'tmp1.nc' + ' ' + self.temp + 'tmp2.nc')
        os.system('cdo setyear,' + str(self.year).zfill(1) + ' ' + self.temp + 'tmp2.nc' + ' ' + self.temp + 'tmp1.nc')

        #os.system('cdo -L -r -f nc4 -z zip_9 -k auto copy ' + self.temp + 'tmp1.nc' + ' ' + self.filename)
        #os.system('rm ' + self.temp + 'tmp1.nc')
        #os.system('rm ' + self.temp + 'tmp2.nc')

        return True

