
debugging=1

### Parameters for local debugging
if debugging==1:
    year    = "2031"
    month   = "01"
    day     = "01"
    month1  = "1"
    INPUT   = '/storage/karger/chelsa_V2/INPUT/'

    TEMP    = '/home/karger/scratch/'
    SRAD    = '/storage/karger/chelsa_V2/OUTPUT_DAILY/srad/'
    CC      = '/storage/karger/chelsa_V2/climatologies/1981-2010/tcc/'
    W5E5    = '/storage/karger/CMIP6_AUX/'
    #CCOVER  = '/mnt/storage/karger/chelsa_V2/OUTPUT_DAILY/tcc/'
    ISIMIP  = '/storage/karger/ISIMIP3b/'
    CMIP6   = '/storage/dabaghch/cmip6/'
    MODEL   = 'MPI-ESM1-2-HR'
    RCP     = 'ssp126'
    lev_low  =  100000
    lev_high =  85000
    OUTPUT  = '/storage/karger/chelsa_V2/ISIMIP3b_BA/' + MODEL + '/' + RCP + '/'
    TEMP = TEMP + '/' + year + month + day + '/'
    dayofyear = "1"
    calcprec=1
    t='1'

    ISIMIP = ISIMIP + RCP + '/' + MODEL + '/'
    os.mkdir(TEMP)