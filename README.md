CHELSA_ISIMIP3b_BA_1km
-----------
This software contains the source code of CHELSA to downscale
gridded climate data to a higher spatial resolutuion. It downscales
min-, max-, and mean temperature, precipitation rate, and solar radiaton.
The code presented here is adapted to the ISIMIP3_BA data as focing.
It is part of the CHELSA Project: (CHELSA, <https://www.chelsa-climate.org/>).


COPYRIGHT
---------
(C) 2021 Dirk Nikolaus Karger


LICENSE
-------
CHELSA_ISIMIP3b_BA_1km is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the
Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

CHELSA_ISIMIP3b_BA_1km is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with CHELSA_ISIMIP3b_BA_1km. If not, see <http://www.gnu.org/licenses/>.



REQUIREMENTS
------------
CHELSA_ISIMIP3b_BA_1km is written in Python 2.7.

All dependencies are resolved in the chelsa_V2.1.ismip.cont singularity container



HOW TO USE
----------
The core function of CHELSA_ISIMIP3b_BA_1km is CHELSA_ISIMIP3b_BA_1km.py. 
It takes several arguments that define the input data location, temporary 
data location, and the output location. Additionally, ssp and model can be 
set, as well as the two pressure levels that are used for the temperature 
lapse rate calculations. These parameters need to be adapted for the respective 
system on which the code is supposed to run. 

Currently, two slurm scripts to parallelize the calculations and set the parameters
are provided. They are adapted for the hyperion cluster at WSL and the
cluster at PIK and can be distinguished by the respective ending (.wsl, .pik) 
These two slurm scripts directly call the CHELSA_ISIMIP3b_BA_1km.py
function using the singularity container. The five parameters that need to be set are:

--array #how many time steps do you want to calculate starting from the start year, counted in days
--partition #what partition do you want to use on your cluster

MODEL #What model needs to be calculated

SSP #Which ssp of the model needs to be used

START #Whats the start year (usually needs to be given as year-1)

an example is given in the example section.

INPUT DATA:
------------
The ISIMIP and CMIP6 input data should be stored in a separate folder. Different models and
ssps can be in the same folder, but only a single timeline should be present for each dataset.
The ingester.py module screens the folder for all files that contain the model name and ssp.
If e.g. the same model and the same ssp is present with another temporal resolution, the 
code will fail.

STATIC INPUT DATA:
------------
The static input data for the algorithm that should be placed in the 'input directory' (argument: '-i','--input')) can be downloaded here:
https://envicloud.wsl.ch/#/?prefix=chelsa%2Fchelsa_V2%2FGLOBAL%2Finput%2F

The daily clear sky solar radiation needed as input can be downloaded here:
https://envicloud.wsl.ch/#/?prefix=chelsa%2Fchelsa_V2%2FGLOBAL%2Fdaily_normals%2Frsdscs%2F

The cloud cover data needed as input can be downloaded here:
https://envicloud.wsl.ch/#/?prefix=chelsa%2Fchelsa_V2%2FGLOBAL%2Fclimatologies%2F1981-2010%2Fclt%2F


ADDITIONAL CMIP6 DATA:
------------
There are additional forcings from CMIP6 that need to be available locally to run CHELSA_ISMIP3b_BA_1km.

the following variables are needed in addition to the ISIMIP3b_BA data:

[variable]_[temporal-resolution]

zg_day

va_day

ua_day

ta_day

clt_day

CITATION:
------------
If you need a citation for the output, please refer to the article describing the high
resolution climatologies:

Karger, D.N., Conrad, O., Böhner, J., Kawohl, T., Kreft, H., Soria-Auza, R.W., Zimmermann, N.E., Linder, P., Kessler, M. (2017). Climatologies at high resolution for the Earth land surface areas. Scientific Data. 4 170122. https://doi.org/10.1038/sdata.2017.122


EXAMPLE: 
------------

To calculate the 01.01.2025 - 02.01.2025 for MPI-ESM1-2-HR ssp126 use:

sbatch --array=1-2 --partition=node CHELSA_ISIMIP3b_BA_1km.slurm.wsl MPI-ESM1-2-HR ssp126 2024

OUTPUT
------------
The output consist of geotiff files. There will be different files for each variable and each day 
separatly.

CONTACT
-------
<dirk.karger@wsl.ch>



AUTHOR
------
Dirk Nikolaus Karger
Swiss Federal Research Institute WSL
Zürcherstrasse 111
8903 Birmensdorf 
Switzerland
